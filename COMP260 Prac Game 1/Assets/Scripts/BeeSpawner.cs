﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{

    public int nBees = 200;
    public BeeMove BeePrefab;
    public Rect spawnRect;
    private float xMin, yMin;
    private float width, height;

    //For spawning more bees on a timer
    public float beePeriod;
    public float minBeePeriod, maxBeePeriod;

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    // Use this for initialization
    void Start()
    {

        //Instantiate Bee
        for (int i = 0; i < nBees; i++)
        {
            BeeMove bee = Instantiate(BeePrefab);
            //Attatches top hierarchy

            bee.transform.parent = transform;

            //Name in order

            bee.gameObject.name = "Bee " + i;
            float x = spawnRect.xMin + Random.value * spawnRect.width;
            float y = spawnRect.yMin + Random.value * spawnRect.height;

            bee.transform.position = new Vector2(x, y);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Random number between 0-200
        float randomNumber = Random.Range(0, 200);
        //Create a bee randomly between maxBeePeriod/minBeePeriod
        if(randomNumber >= minBeePeriod && randomNumber <= maxBeePeriod)
        {
            spawnBee();
        }


        //Create a bee every beePeriod seconds which is 5 second    s at the moment
        /*(if(Time.time >= beePeriod)
        {
            spawnBee();
            beePeriod =  beePeriod + 5 ;
        }*/
      

    }

    void spawnBee()
    {
        BeeMove bee = Instantiate(BeePrefab);
        bee.gameObject.name = "Bee " + beePeriod;
        bee.transform.parent = transform;
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);

            Vector2 v = (Vector2)child.position - centre;

            if (v.magnitude <= radius){
                Destroy(child.gameObject);
            }
        }
    }
}
