﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    //Public parameters with set values
    public ParticleSystem explosionPrefab;
    // private state
    private float speed;
    private float turnSpeed;

    private Transform target;
    private Vector2 heading;


    // Use this for initialization
    void Start () {
        // find the player to set the target
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;
        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);
    }

    private void OnDrawGizmos()
    {
        //draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        //draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

    // Update is called once per frame
    void Update() {
        Vector2 direction = target.position - transform.position;
        float angle = turnSpeed * Time.deltaTime;

        if (direction.IsOnLeft(heading))
        {
            heading = heading.Rotate(angle);
        }
        else
        {
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);

        /*
            if (target.position.magnitude > target2.position.magnitude)
            {
                Vector2 direction = target2.position - transform.position;
                float angle = turnSpeed * Time.deltaTime;

                if (direction.IsOnLeft(heading))
                {
                    heading = heading.Rotate(angle);
                }
                else
                {
                    heading = heading.Rotate(-angle);
                }
                transform.Translate(heading * speed * Time.deltaTime);

            }
            else
            {
                Vector2 direction = target.position - transform.position;
                float angle = turnSpeed * Time.deltaTime;

                if (direction.IsOnLeft(heading))
                {
                    heading = heading.Rotate(angle);
                }
                else
                {
                    heading = heading.Rotate(-angle);
                }
                transform.Translate(heading * speed * Time.deltaTime);
            }*/
    }
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);
    }
}
