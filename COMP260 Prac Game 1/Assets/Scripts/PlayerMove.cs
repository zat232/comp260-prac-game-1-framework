﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 3.0f; // in metres/second/second
    public float brake = 5.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float turnSpeed = 30.0f; //in degrees/second
    public float destroyRadius = 1.0f;

    public BeeSpawner beeSpawner;

    void Start()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }

    void Update()
    {
        //controls for turning
        float turn = Input.GetAxis("Horizontal");
        transform.Rotate(0, 0, -turn * (speed* turnSpeed) * Time.deltaTime);
        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            if (speed > 0)
            {
                speed = Mathf.Clamp(speed - brake * Time.deltaTime,0 ,maxSpeed);
            }
            else if (speed < 0)
            {
                speed = Mathf.Clamp(speed + brake * Time.deltaTime,-maxSpeed,0);
            }
        }
        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);

        //Destroy bees
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }
    }
}