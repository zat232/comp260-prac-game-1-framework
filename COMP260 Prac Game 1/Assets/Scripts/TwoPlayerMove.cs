﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPlayerMove : MonoBehaviour
{
    public Transform player;
    public string playerX = "";
    public string playerY = "";
    public float maxSpeed = 5.0f;


    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
            Vector2 direction;
            direction.x = Input.GetAxis(playerX);
            direction.y = Input.GetAxis(playerY);
            Vector2 velocity = direction * maxSpeed;
            transform.Translate(velocity * Time.deltaTime);


    }

}
